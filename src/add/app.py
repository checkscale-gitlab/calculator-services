from flask import Flask, request


def create_app():
    app = Flask(__name__)

    @app.route("/", methods=['GET'])
    def add():
        app.logger.info(request.args)
        var_1 = int(request.args.get('arg1'))
        var_2 = int(request.args.get('arg2'))
        return str(var_1 + var_2)

    return app


if __name__ == '__main__':
    add_app = create_app()
    add_app.run(host='0.0.0.0', debug=True)
