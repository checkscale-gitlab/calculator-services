import pytest

from src.add.app import create_app

_SERVICE_ENDPOINT_URL = '/?arg1={value_1}&arg2={value_2}'


@pytest.fixture
def client():
    app = create_app()
    with app.test_client() as client:
        yield client


def test_add_route_returns_valid_value(client):
    get_url = _SERVICE_ENDPOINT_URL.format(value_1=7, value_2=8)

    response = client.get(get_url)

    assert response.data == b'15'
    assert response.status_code == 200
